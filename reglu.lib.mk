CC ?= cc
AR ?= ar
LIB ?= libmain
PREFIX ?= /usr
SRC ?= $(LIB).c
OBJS = $(SRC:.c=.o)

$(LIB): $(LIB).so $(LIB).a

$(LIB).so: $(OBJS)
	$(CC) -shared -fPIC $(CFLAGS) $(CPPFLAGS) $(LDFLAGS) $(OBJS) -o $(LIB).so

$(LIB).a: $(OBJS)
	$(AR) rcs $(LIB).a $(OBJS)

%.o: %.c
	$(CC) -c $(CFLAGS) $(CPPFLAGS) $(LDFLAGS) $< -o $@ 
