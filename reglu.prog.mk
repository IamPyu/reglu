CC ?= cc
PROG ?= main
PREFIX ?= /usr
SRC ?= $(PROG).c
OBJS = $(SRC:.c=.o)

$(PROG): $(OBJS)
	$(CC) $(CFLAGS) $(CPPFLAGS) $(LDFLAGS) $(OBJS) -o $(PROG)

%.o: %.c
	$(CC) -c $(CFLAGS) $(CPPFLAGS) $(LDFLAGS) $< -o $@

install: $(PROG)
	cp $(PROG) $(PREFIX)/bin

uninstall:
	rm -f $(PREFIX)/bin/$(PROG)
