PREFIX = /usr
MAKEFILES = reglu.prog.mk reglu.lib.mk

.PHONY: install uninstall help
help:
	@echo "Targets: install uninstall"

install:
	mkdir -p $(PREFIX)/share/mk 
	cp $(MAKEFILES) $(PREFIX)/share/mk

uninstall:
	rm -f $(PREFIX)/share/mk/reglu.*.mk


